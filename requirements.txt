# List of all dependencies for navarp

PyQt5
numpy >= 1.16.0
scipy >= 1.3.0
matplotlib >= 3.5.2
h5py >= 2.9.0
pyyaml
Click
colorcet
igor2
