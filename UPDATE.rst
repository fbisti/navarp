.. highlight:: shell

============
Update
============

To update the last stable NavARP package do::

    pip install navarp -U

For the last version still under development run::

    pip install https://gitlab.com/fbisti/navarp/-/archive/develop/navarp-develop.zip -U