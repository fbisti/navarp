.. highlight:: shell

Installation
============

Linux Debian/Ubuntu/Mint
--------------------------------------

Thanks to the "Data Reduction and Analysis Group at Synchrotron SOLEIL", 
NavARP is available for linux as Debian/Ubuntu/Mint package since the version 1.6.0, 
and can be installed with:

	apt install python3-navarp

Any operating system using Miniforge
--------------------------------------

Miniforge is a lightweight distribution of Conda, proving a minimal environment 
with Conda, Python, pip, and the Conda-Forge channel preconfigured. 
It is an alternative to Anaconda or Miniconda. 
Download the last version of Miniforge from 
[here](https://conda-forge.org/download/ 'https://conda-forge.org/download/'). 
Then install it considering the following recommendation:
1. do it without administrator priviledges (select "Just Me" during the installation 
process), as the other option can make installing packages more difficult later on;
2. tick "Add Miniforge3 to my Path", otherwise Conda would not work from any Terminal Window.

Command line interface
****************************************

After Miniforge installation, few commands must be run in the proper **command line 
interface** before being able to use the NavARP package, and this **command 
line interface** depends on the operating system. 

The **command line interface** corresponds to the **terminal** on Unix-like platforms 
(macOS or Linux) and to the **Miniforge Prompt** on Windows (which is accessible from 
the **Start menu** after searching for **Miniforge Prompt**). 

Regarding **macOS**, it may occur that following Miniforge installation, the default 
Python version available via the terminal is still the one from macOS 
(not Miniforge-related). To Verify it is not the case check if the command line now 
begins with ``(base)``. If it is not the case, type and execute ``conda activate``; 
``(base)`` ought to follow.

Prepare the correct python environment
****************************************

NavARP is a python package requiring different python libraries for working correctly. 
Therefore, before its installation it is needed to prepare a dedicated python 
environment for it, which can be called as ``navarp-env``. 
To do that run the following commands in the appropriate **command-line interface**::

    conda create --name navarp-minienv numpy scipy matplotlib colorcet h5py pyqt=5 jupyter ipympl pyyaml click igor2

Then activate the created environment using the following command::

    conda activate navarp-env

Install NavARP in the created python environment
**************************************************

After the last command of the previous section, the command line should begin with 
``(navarp-env)`` instead of ``(base)``. 
If it is the case, you can install NavARP. 
You can chose to install the last stable version with::

	pip install navarp

Or the new version still under development with::

	pip install https://gitlab.com/fbisti/navarp/-/archive/develop/navarp-develop.zip

Running NavARP 
****************
After the previous steps, NavARP can run through the **command line interface**, 
but only once inside the proper created environment ``(navarp-env)``! 
Rember to check if the command line begin with ``(navarp-env)``, rather than ``(base)``. 
If it is not the case run::

    conda activate navarp-env

So, to open the NavARP graphical user interface run the following command::

    navarp

For getting familiar with the libraries, launch Jupyter Notebook from 
the **command line interface**, always inside ``(navarp-env)``, running the command::

	jupyter notebook

Then open some examples which you can find in the 
[example folder](https://gitlab.com/fbisti/navarp/-/tree/master/example 
'https://gitlab.com/fbisti/navarp/-/tree/master/example'). 


